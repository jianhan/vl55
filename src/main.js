// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import store from './store'
import VuexFlash from 'vuex-flash'
import VeeValidate from 'vee-validate'

// loading static assets
require('./assets/css/bootstrap.css')
require('./assets/css/style.default.css')
require('./assets/css/custom.css')
require('./assets/css/font-awesome.min.css')
require('tether')

Vue.use(VeeValidate)
Vue.use(BootstrapVue)
Vue.config.productionTip = false
Vue.use(VuexFlash)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
