import * as env from '../.env'

export default {
  methods: {
    setToken: (accessToken, expiration) => {
      localStorage.setItem(env.ACCESS_TOKEN_KEY, accessToken)
      localStorage.setItem(env.EXPIRATION_KEY, Date.now() + expiration)
    },
    getToken: function () {
      let accessToken = localStorage.getItem(env.ACCESS_TOKEN_KEY)
      let expiration = localStorage.getItem(env.EXPIRATION_KEY)
      if (!accessToken || !expiration) {
        return null
      }
      if (Date.now() > parseInt(expiration)) {
        this.removeToken()
        return null
      }
      return accessToken
    },
    removeToken: () => {
      localStorage.removeItem(env.ACCESS_TOKEN_KEY)
      localStorage.removeItem(env.EXPIRATION_KEY)
    },
    isAuthenticated: function () {
      if (this.getToken())
        return true
      else
        return false
    },
    logout: function () {
      this.removeToken()
    }
  },
  mounted() {
    let ClientOAuth2 = require('client-oauth2')
    this.clientOAuth2 = new ClientOAuth2({
      clientId: env.CLIENT_ID,
      clientSecret: env.CLIENT_SECRET,
      accessTokenUri: env.TOKEN_URL,
      authorizationUri: env.AUTHORIZE_URL,
      scopes: ['']
    })
  }
}
