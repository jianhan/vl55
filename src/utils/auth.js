import * as env from '../.env'

export function setToken (accessToken, expiration) {
  localStorage.setItem(env.ACCESS_TOKEN_KEY, accessToken)
  localStorage.setItem(env.EXPIRATION_KEY, Date.now() + expiration)
}

export function getToken () {
  let accessToken = localStorage.getItem(env.ACCESS_TOKEN_KEY)
  let expiration = localStorage.getItem(env.EXPIRATION_KEY)
  if (!accessToken || !expiration) {
    return null
  }
  if (Date.now() > parseInt(expiration)) {
    this.removeToken()
    return null
  }
  return accessToken
}

export function removeToken () {
  localStorage.removeItem(env.ACCESS_TOKEN_KEY)
  localStorage.removeItem(env.EXPIRATION_KEY)
}

export function getUser () {
  let authUser = localStorage.getItem(env.AUTH_USER_KEY)
  if (!authUser) {
    return false
  }
  return JSON.parse(authUser)
}

export function setUser (user) {
  localStorage.setItem(env.AUTH_USER_KEY, JSON.stringify(user))
}

export function removeUser (user) {
  localStorage.removeItem(env.AUTH_USER_KEY)
}

export function isAuthenticated () {
  if (getToken()) {
    return true
  } else {
    return false
  }
}
