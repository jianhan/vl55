import axios from 'axios'
import * as env from '../.env'

let accessToken = localStorage.getItem(env.ACCESS_TOKEN_KEY)
let HTTP = false

if (accessToken != null) {
  HTTP = axios.create({
    headers: {
      Authorization: 'Bearer ' + accessToken
    }
  })
} else {
  HTTP = axios.create()
}

export default HTTP
