import * as types from './mutation-types'
import _ from 'lodash'

import * as authUtils from '../../utils/auth'

const state = {
  authUser: false,
  isAuthenticating: false,
  message: false
}

// getters
const getters = {
  isAuthenticated: state => {
    if (_.isEmpty(state.authUser)) {
      return false
    }
    return true
  },
  hasAuthMessage: state => {
    if (!state.message) {
      return false
    }
    return true
  }
}

// actions
const actions = {
}

// mutations
const mutations = {
  [types.SET_ISAUTHENTICATING] (state, isAuthenticating) {
    state.isAuthenticating = isAuthenticating
  },
  [types.SET_AUTH_USER] (state, authUser) {
    state.authUser = authUser
  },
  [types.INIT_AUTH] (state) {
    let authUser = authUtils.getUser()
    if (!_.isNil(authUser)) {
      state.authUser = authUser
    } else {
      state.authUser = false
    }
  },
  [types.SET_AUTH_MESSAGE] (state, message) {
    state.message = message
  },
  [types.CLEAR_AUTH_MESSAGE] (state) {
    state.message = false
  },
  [types.UNAUTHENTICATE] (state) {
    state.authUser = false
    authUtils.removeToken()
    authUtils.removeUser()
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
