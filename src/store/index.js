import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import auth from './modules/auth'
import { createFlashStore } from 'vuex-flash'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    flashMessage: null
  },
  actions,
  getters,
  mutations,
  modules: {
    auth
  },
  plugins: [
    createFlashStore()
  ]
})
