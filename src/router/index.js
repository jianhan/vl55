import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Auth from '@/components/auth/Auth.vue'
import Login from '@/components/auth/Login.vue'
import Admin from '@/components/admin/Admin.vue'
import AdminDashboard from '@/components/admin/Dashboard.vue'
import store from '../store'
import * as authUtils from '../utils/auth'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/auth',
      component: Auth,
      meta: {},
      children: [
        {
          path: 'login',
          name: 'login',
          component: Login,
          meta: {}
        }
      ]
    },
    {
      path: '/admin',
      component: Admin,
      name: 'admin',
      meta: {},
      beforeEnter: (to, from, next) => {
        if (!authUtils.isAuthenticated()) {
          store.commit('FLASH/SET_FLASH', {message: 'Please login to continue', variant: 'danger'})
          next({path: '/auth/login'})
        } else {
          next()
        }
      },
      children: [
        {
          path: 'dashboard',
          component: AdminDashboard,
          name: 'admin_dashboard',
          meta: {}
        }
      ]
    }
  ]
})
